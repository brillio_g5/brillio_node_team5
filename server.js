const express = require("express");
const app = express();
const router = require("./src/routes/router");
const sequelize = require("./src/config/dbconnect");
require("./src/model/siteService");

const PORT = process.env.PORT || 8080;

app.use(express.json());

app.use("/site", router);

sequelize
  .sync()
  .then(() => {
    app.listen(PORT, () => {
      console.log(`server started at ${PORT}`);
    });
  })
  .catch((error) => {
    console.error("Unable to sync models: ", error);
  });
