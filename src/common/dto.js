const { v4: uuid } = require("uuid");

exports.siteServiceDto = (req) => {
  const { body } = req;
  return {
    id: uuid(),
    ticket_number: body.ticketNumber,
    name: body.name,
    equipment_category: body.equipmentCategory,
    location: body.location,
    description: body.description,
    user_mail: body.userMail,
    owner_mail: body.ownerMail,
  };
};
