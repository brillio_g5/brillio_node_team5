exports.siteServiceDao = (item) => {
  return {
    id: item.id,
    ticketNumber: item.ticket_number,
    name: item.name,
    equipmentCategory: item.equipment_category,
    location: item.location,
    description: item.description,
    userMail: item.user_mail,
    ownerMail: item.owner_mail,
  };
};
