const { siteServiceDao } = require("../common/dao");
const { siteServiceDto } = require("../common/dto");
const SITEDETAILS = require("../model/siteService");

const getSite = async (req, res) => {
  try {
    const allSite = await SITEDETAILS.findAll();
    const modifiedData = await allSite.map((item) => siteServiceDao(item));
    res.json(modifiedData);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving datas.",
    });
  }
};

const getSiteById = async (req, res) => {
  const { id } = req.params;
  try {
    const allSite = await SITEDETAILS.findOne({ id: id });
    const modifiedData = await siteServiceDao(allSite);
    res.json(modifiedData);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving datas.",
    });
  }
};

const postSite = async (req, res) => {
  try {
    let data = await SITEDETAILS.create(siteServiceDto(req));
    const modifiedData = await siteServiceDao(data);
    res.json(modifiedData);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while creating the data.",
    });
  }
};

const patchSite = async (req, res) => {
  const { id } = req.params;
  const body = req.body;
  try {
    let data = await SITEDETAILS.findOne({ where: { id: id } });
    const daoData = await siteServiceDao(data);
    if (daoData) {
      const patchData = { ...daoData, ...body };
      req.body = patchData;
      const modifiedData = await siteServiceDto(req);
      await data.update(modifiedData);
      res.json(await siteServiceDao(data));
      return;
    }
    res.sendStatus(404);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while updating the data.",
    });
  }
};

const deleteSite = async (req, res) => {
  const { id } = req.params;
  try {
    let data = await SITEDETAILS.findOne({ where: { id: id } });
    console.log(data, "data");
    if (data) {
      data.destroy();
      res.send("deleted succesfully");
      return;
    }
    res.sendStatus(404);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while deleting the data.",
    });
  }
};

module.exports = {
  getSite,
  postSite,
  patchSite,
  deleteSite,
  getSiteById,
};
