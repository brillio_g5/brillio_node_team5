const { DataTypes } = require("sequelize");
const sequelize = require("../config/dbconnect");

const SITEDETAILS = sequelize.define("site_details", {
  id: {
    type: DataTypes.STRING,
    allowNull: false,
    primaryKey: true,
  },
  ticket_number: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  equipment_category: {
    type: DataTypes.STRING,
  },
  location: {
    type: DataTypes.STRING,
  },
  description: {
    type: DataTypes.STRING,
  },
  user_mail: {
    type: DataTypes.STRING,
  },
  owner_mail: {
    type: DataTypes.STRING,
  },
});

module.exports = SITEDETAILS;
