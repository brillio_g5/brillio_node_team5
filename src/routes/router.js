const router = require("express").Router();
const siteServiceController = require("../controller/siteServiceController");

router.get("/", siteServiceController.getSite);
router.get("/:id", siteServiceController.getSiteById);
router.post("/create", siteServiceController.postSite);
router.patch("/update/:id", siteServiceController.patchSite);
router.delete("/delete/:id", siteServiceController.deleteSite);

module.exports = router;
