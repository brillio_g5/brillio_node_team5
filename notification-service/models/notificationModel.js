const db = require('../db');

const createNotification = async (notification) => {
    const sql = `
        INSERT INTO notifications (ticketNumber, ticketDetails, changeFlag, userMail, ownerMail)
        VALUES (?, ?, ?, ?, ?)
    `;
    const [result] = await db.execute(sql, [
        notification.ticketNumber,
        notification.ticketDetails,
        notification.changeFlag,
        notification.userMail,
        notification.ownerMail
    ]);
    return result;
};

const updateNotification = async (id, notification) => {
    const sql = `
        UPDATE notifications
        SET ticketNumber = ?, ticketDetails = ?, changeFlag = ?, userMail = ?, ownerMail = ?
        WHERE id = ?
    `;
    const [result] = await db.execute(sql, [
        notification.ticketNumber,
        notification.ticketDetails,
        notification.changeFlag,
        notification.userMail,
        notification.ownerMail,
        id
    ]);
    return result;
};

const deleteNotification = async (id) => {
    const sql = `DELETE FROM notifications WHERE id = ?`;
    const [result] = await db.execute(sql, [id]);
    return result;
};

const getAllNotifications = async () => {
    const sql = `SELECT * FROM notifications`;
    const [result] = await db.execute(sql);
    return result;
};

module.exports = {
    createNotification,
    updateNotification,
    deleteNotification,
    getAllNotifications
};
