const {
    createNotification,
    updateNotification,
    deleteNotification,
    getAllNotifications
} = require('../models/notificationModel');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'rajeshbabu4304@gmail.com',
        pass: 'sapphireA#1212'
    }
});

const ownerMail = 'rajeshbabu0671@gmail.com';

const createNotificationHandler = async (req, res) => {
    const { ticketNumber, ticketDetails, changeFlag, userMail } = req.body;
    const notification = { ticketNumber, ticketDetails, changeFlag, userMail, ownerMail };

    try {
        await createNotification(notification);

        const mailOptions = {
            from: 'rajeshbabu4304@gmail.com',
            to: [userMail, ownerMail],
            subject: `Ticket ${changeFlag}`,
            text: `Ticket Number: ${ticketNumber}\nTicket Details: ${ticketDetails}\nStatus: ${changeFlag}`
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Email sent: ' + info.response);
        });

        res.status(201).json({ message: 'Notification created and email sent successfully.' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'An error occurred while creating the notification.' });
    }
};

const updateNotificationHandler = async (req, res) => {
    const { id } = req.params;
    const { ticketNumber, ticketDetails, changeFlag, userMail } = req.body;
    const notification = { ticketNumber, ticketDetails, changeFlag, userMail, ownerMail };

    try {
        await updateNotification(id, notification);

        const mailOptions = {
            from: 'rajeshbabu4304@gmail.com',
            to: [userMail, ownerMail],
            subject: `Ticket ${changeFlag}`,
            text: `Ticket Number: ${ticketNumber}\nTicket Details: ${ticketDetails}\nStatus: ${changeFlag}`
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Email sent: ' + info.response);
        });

        res.status(200).json({ message: 'Notification updated and email sent successfully.' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'An error occurred while updating the notification.' });
    }
};

const deleteNotificationHandler = async (req, res) => {
    const { id } = req.params;

    try {
        await deleteNotification(id);
        res.status(200).json({ message: 'Notification deleted successfully.' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'An error occurred while deleting the notification.' });
    }
};

const getAllNotificationsHandler = async (req, res) => {
    try {
        const notifications = await getAllNotifications();
        res.status(200).json(notifications);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'An error occurred while fetching the notifications.' });
    }
};

module.exports = {
    createNotificationHandler,
    updateNotificationHandler,
    deleteNotificationHandler,
    getAllNotificationsHandler
};
