const express = require('express');
const bodyParser = require('body-parser');
const notificationRoutes = require('./routes/notificationRoutes');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use('/api', notificationRoutes);

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
