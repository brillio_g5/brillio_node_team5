const express = require('express');
const router = express.Router();
const {
    createNotificationHandler,
    updateNotificationHandler,
    deleteNotificationHandler,
    getAllNotificationsHandler
} = require('../controllers/notificationController');

router.post('/notifications', createNotificationHandler);
router.put('/notifications/:id', updateNotificationHandler);
router.delete('/notifications/:id', deleteNotificationHandler);
router.get('/notifications', getAllNotificationsHandler);

module.exports = router;
